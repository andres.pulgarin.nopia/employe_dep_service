const { Pool } = require('pg');

const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    password: 'admin',
    database: 'Admin_Empleados',
    port: '5434'
});

const getEmpleados = async (req, res) => {
    const response = await pool.query('SELECT * FROM empleado ORDER BY codigo ASC');
    res.status(200).json(response.rows);
};

const getEmpleadosById = async (req, res) => {
    const id = parseInt(req.params.id);
    const response = await pool.query('SELECT * FROM empleado WHERE codigo = $1', [id]);
    res.json(response.rows);
};

const createEmpleados = async (req, res) => {
    const { nit, nombre, apellido1, apellido2, codigo_departamento } = req.body;
    const response = await pool.query('INSERT INTO empleado (nit, nombre, apellido1, apellido2, codigo_departamento) VALUES ($1, $2, $3, $4, $5)', [nit, nombre, apellido1, apellido2, codigo_departamento]);
    res.json({
        message: 'Empleado agregado con exito',
        body: {
            user: {nit, nombre, apellido1, apellido2, codigo_departamento}
        }
    })
};

const updateEmpleados = async (req, res) => {
    const id = parseInt(req.params.id);
    const {nit, nombre, apellido1, apellido2, codigo_departamento, codigo } = req.body;

    const response =await pool.query('UPDATE empleado SET nit = $1, nombre = $2, apellido1 = $3, apellido2 = $4, codigo_departamento = $5 WHERE codigo = $6', [
        nit, nombre, apellido1, apellido2, codigo_departamento, id
    ]);
    res.json('Empleado actualizado con exito');
};

const deleteEmpleados = async (req, res) => {
    const id = parseInt(req.params.id);
    await pool.query('DELETE FROM empleado where codigo = $1', [
        id
    ]);
    res.json(`Empleado ${id} eliminado con exito`);
};

module.exports = {
    getEmpleados,
    getEmpleadosById,
    createEmpleados,
    updateEmpleados,
    deleteEmpleados
};