const { Pool } = require('pg');

const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    password: 'admin',
    database: 'Admin_Empleados',
    port: '5434'
});

const getdepartamentos = async (req, res) => {
    const response = await pool.query('SELECT * FROM departamento ORDER BY codigo_departamento ASC');
    res.status(200).json(response.rows);
};

const getdepartamentosById = async (req, res) => {
    const id = parseInt(req.params.id);
    const response = await pool.query('SELECT * FROM departamento WHERE codigo_departamento = $1', [id]);
    res.json(response.rows);
};

const createdepartamentos = async (req, res) => {
    const { nombre, presupuesto } = req.body;
    const response = await pool.query('INSERT INTO departamento (nombre, presupuesto ) VALUES ($1, $2)', [nombre, presupuesto ]);
    res.json({
        message: 'departamento agregado con exito',
        body: {
            user: {nombre, presupuesto }
        }
    })
};

const updatedepartamentos = async (req, res) => {
    const id = parseInt(req.params.id);
    const {nombre, presupuesto} = req.body;

    const response =await pool.query('UPDATE departamento SET nombre = $1, presupuesto = $2 WHERE codigo_departamento= $3', [
        nombre, presupuesto, id
    ]);
    res.json('departamento actualizado con exito');
};

const deletedepartamentos = async (req, res) => {
    const id = parseInt(req.params.id);
    await pool.query('DELETE FROM departamento where codigo_departamento= $1', [
        id
    ]);
    res.json(`departamento ${id} eliminado con exito`);
};

module.exports = {
    getdepartamentos,
    getdepartamentosById,
    createdepartamentos,
    updatedepartamentos,
    deletedepartamentos
};