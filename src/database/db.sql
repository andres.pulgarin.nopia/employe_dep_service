-- Crear Base de datos
CREATE DATABASE Admin_Empleados;

-- Crear tabla 'departamento'

CREATE TABLE IF NOT EXISTS departamento
(
    codigo_departamento integer NOT NULL DEFAULT nextval('departamento_codigo_departamento_seq'::regclass),
    nombre character varying(100) COLLATE pg_catalog."default",
    presupuesto double precision,
    CONSTRAINT pk_codigo_departamentos PRIMARY KEY (codigo_departamento)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS departamento
    OWNER to postgres;

-- Insertar registros de prueba para tabla departamento

INSERT INTO public.departamento(
	codigo_departamento, nombre, presupuesto)
	VALUES (1, "Cauca", 1.2);

INSERT INTO public.departamento(
	codigo_departamento, nombre, presupuesto)
	VALUES (2, "Meta", 45.2);

-- Crear tabla 'empleado'

CREATE TABLE IF NOT EXISTS empleado
(
    codigo integer NOT NULL DEFAULT nextval('empleado_codigo_seq'::regclass),
    nit character varying(9) COLLATE pg_catalog."default",
    nombre character varying(100) COLLATE pg_catalog."default",
    apellido1 character varying(100) COLLATE pg_catalog."default",
    apellido2 character varying(100) COLLATE pg_catalog."default",
    codigo_departamento integer NOT NULL,
    CONSTRAINT pk_codigo_empleado PRIMARY KEY (codigo),
    CONSTRAINT fk_emp_cod_dep FOREIGN KEY (codigo_departamento)
        REFERENCES departamento (codigo_departamento) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS empleado
    OWNER to postgres;

-- INSERTAR REGISTROS DE PRUEBA 

INSERT INTO empleado (nit, nombre, apellido1, apellido2, codigo_departamento)
    VALUES ('45', 'Juan','Lopez', 'Trujillo', 1);

INSERT INTO empleado(
	codigo, nit, nombre, apellido1, apellido2, codigo_departamento)
	VALUES ('46', 'Pedro','Garcia', 'Naranjo', 2);

select * from empleado;
