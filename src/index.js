const express = require('express');

const app = express();

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Routes
app.use(require('./routes/emp_routes'));
app.use(require('./routes/dep_routes'));

app.listen(1234);
console.log('Server on port', 1234);