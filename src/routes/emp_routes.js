const { Router } = require('express');
const router = Router();

const { getEmpleados, getEmpleadosById, createEmpleados, updateEmpleados, deleteEmpleados } = require('../controllers/emp_controller');

router.get('/empleados', getEmpleados);
router.get('/empleados/:id', getEmpleadosById);
router.post('/empleados', createEmpleados);
router.put('/empleados/:id', updateEmpleados)
router.delete('/empleados/:id', deleteEmpleados);

module.exports = router;