const { Router } = require('express');
const router = Router();

const { getdepartamentos, getdepartamentosById, createdepartamentos, updatedepartamentos, deletedepartamentos } = require('../controllers/dep_controller');

router.get('/departamentos', getdepartamentos);
router.get('/departamentos/:id', getdepartamentosById);
router.post('/departamentos', createdepartamentos);
router.put('/departamentos/:id', updatedepartamentos);
router.delete('/departamentos/:id', deletedepartamentos);

module.exports = router;